<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION["id"]))) {
header('Location: http://marinelounge.in/google_login.php');
} 
?>
<!DOCTYPE html>
<html>
<title>Marine Lounge</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=icon href="marinelounge_icon.png">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}

</style>
<body>

<!-- Navbar -->
<ul class="w3-navbar w3-blue w3-card-2 w3-top w3-left-align w3-large">
  <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large w3-hover-white w3-large w3-blue" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
  </li>
  <li><a href="index.php" class="w3-padding-large w3-white">Home</a></li>
				
				<li class="w3-hide-small">
					<a href="about.php" class="w3-padding-large w3-hover-white">About Us</a>
				</li >
				<li class="w3-hide-small">
					<a href="contact.php" class="w3-padding-large w3-hover-white">Contact</a>
				</li>
				<li class="w3-hide-small">
					<a href="rules.php" class="w3-padding-large w3-hover-white">Rules</a>
				</li>
				<li class = "w3-hide-small" style = "float:right;">
					<?php
						if(!(isset($_SESSION["id"]))) {
					echo '<a href="google_login.php" class="w3-padding-large w3-hover-white">Login</a>';
					} else {
					echo '<a href="dashboard.php" class = "w3-padding-large w3-hover-white"><img src = "'.$_SESSION["picture_url"].'" style = "border-radius:50%;" height = "25"></img>&emsp;Dashboard</a>';
					}
					?>
				</li>
</ul>


<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-top" style="margin-top:51px;">
  <ul class="w3-navbar w3-left-align w3-large w3-black">
    <li class="w3-padding-large">
					<a href="about.php">About Us</a>
				</li>
				<li class="w3-padding-large">
					<a href="contact.php">Contact</a>
				</li>
				<li class="w3-padding-large">
					<a href="rules.php">Rules</a>
				</li>
				<li class="w3-padding-large">
					<?php
						if(!(isset($_SESSION["id"]))) {
					echo '<a href="google_login.php" class="w3-padding-large">Login</a>';
					} else {
					echo '<a href="dashboard.php" class = "w3-padding-large"><img src = "'.$_SESSION["picture_url"].'" style = "border-radius:50%;" height = "25"></img>&emsp;Dashboard</a>';
					}
					?>
				</li>
  </ul>
</div>
<div class="w3-row-padding w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-twothird">
<?php
$content = pg_escape_string($_POST['content']);
$category = pg_escape_string($_POST['category']); 
$submittedby = pg_escape_string($_SESSION['id']); 
$title = pg_escape_string($_POST['title']); 
$thumbnail = pg_escape_string($_POST['thumbnail']);  
$firstimage = pg_escape_string($_POST['image_1']); 
$secondimage = pg_escape_string($_POST['image_2']); 
$video = pg_escape_string($_POST['video']); 
  
$dsn = "pgsql:host=$host;port=5432;dbname=$db;user=$username;password=$password";
 
try{
	// create a PostgreSQL database connection
	$conn = new PDO($dsn);
 
	// display a message if connected to the PostgreSQL successfully
	if($conn){
	}
}catch (PDOException $e){
	// report error message
	echo $e->getMessage();
}
    $sql_query = "INSERT INTO articles (category,content,title,submittedby,thumbnail,featured,ok,firstimage,secondimage,video,popular,favourite) VALUES ('".$category."','".$content."','".$title."','".$submittedby."','".$thumbnail."',0,0,'".$firstimage."','".$secondimage."','".$video."',0,0)";
     try{
	$stmt = $conn->query($sql_query);
 
	if($stmt === false){
		die("Error executing the query: $sql_query");
	}
}catch (PDOException $e){
	echo 'error occurred';
	echo $e->getMessage();
}
echo "<h1>Your article has been submitted</h1>";
echo "<h3>When the admin approves your article then it will be visible on the website</h3>";
echo "<h3>This will usually take 24 hours</h3>";
echo "<h3>Thank you</h3>";

$conn = null;
$stmt = null;
?> 
</div>
</div>
</div>
<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>
</body>
</html>