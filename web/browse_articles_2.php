<?php
session_start();
require_once('includes/config.php');
?>
<!DOCTYPE html>
<html>
<head>
<title>Marine Lounge</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=icon href="marinelounge_icon.png">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta property="fb:pages" content="270738909976749" />
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
footer {
background-color:lightgrey;
-webkit-box-shadow: 0px -4px 3px rgba(50, 50, 50, 0.4);
}
</style>
</head>
<body>

<!-- Navbar -->
<ul class="w3-navbar w3-blue w3-card-2 w3-top w3-left-align w3-large">
  <li class="w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large w3-hover-white w3-large w3-blue" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
  </li>
  <li><a href="index.php" class="w3-padding-large w3-white">Home</a></li>
				
				<li class="w3-hide-small w3-hide-medium">
					<a href="about.php" class="w3-padding-large w3-hover-white">About Us</a>
				</li >
				<li class="w3-hide-small w3-hide-medium">
					<a href="contact.php" class="w3-padding-large w3-hover-white">Contact</a>
				</li>
				<li class="w3-hide-small w3-hide-medium">
					<a href="rules.php" class="w3-padding-large w3-hover-white">Rules</a>
				</li>
				<li class = "w3-hide-small w3-hide-medium" style = "float:right;">
					<?php
						if(!(isset($_SESSION["id"]))) {
					echo '<a href="google_login.php" class="w3-padding-large w3-hover-white">Login</a>';
					} else {
					echo '<a href="dashboard.php" class = "w3-padding-large w3-hover-white"><img src = "'.$_SESSION["picture_url"].'" style = "border-radius:50%;" height = "25"></img>&emsp;Dashboard</a>';
					}
					?>
				</li>
</ul>


<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-top" style="margin-top:51px;">
  <ul class="w3-navbar w3-left-align w3-large w3-black">
    <li class="w3-padding-large">
					<a href="about.php">About Us</a>
				</li>
				<li class="w3-padding-large">
					<a href="contact.php">Contact</a>
				</li>
				<li class="w3-padding-large">
					<a href="rules.php">Rules</a>
				</li>
				<li class="w3-padding-large">
					<?php
						if(!(isset($_SESSION["id"]))) {
					echo '<a href="google_login.php" class="w3-padding-large">Login</a>';
					} else {
					echo '<a href="dashboard.php" class = "w3-padding-large"><img src = "'.$_SESSION["picture_url"].'" style = "border-radius:50%;" height = "25"></img>&emsp;Dashboard</a>';
					}
					?>
				</li>
  </ul>
</div>

	<hr><br>
	<p>Choose a category</p>
	<form action = "browse_articles_2.php" method = "post">
	<select name="category" placeholder = "category" required>
						<option value="Naval Arch">Naval Arch</option>
						<option value="Newsletter">Newsletter</option>
						<option value="Careers">Careers</option>
						<option value="Miscellaneous">Miscellaneous</option>
						<option value="Life Story">Life Story</option>
						<option value="Marine Engineering">Marine Engineering</option> 
						<option value="Off Shore Engineering">Off Shore Engineering</option>
						<option value="Ocean Engineering">Ocean Engineering</option> 
						<option value="Maritime Law">Maritime Law</option> 
						<option value="Maritime Piracy">Maritime Piracy</option>
				</select>
	<input type="submit" name="submit" value="Submit" tabindex="5"><br><br>
	</form>
	
		<?php
		echo '<button class = "w3-btn w3-black">'.$_POST["category"].'</button>
	<hr>';

// Create connection
$dsn = "pgsql:host=$host;port=5432;dbname=$db;user=$username;password=$password";
try{
	// create a PostgreSQL database connection
	$conn = new PDO($dsn);
 
	// display a message if connected to the PostgreSQL successfully
	if($conn){
	}
}catch (PDOException $e){
	// report error message
	echo $e->getMessage();
}

$sql_query = "SELECT id,thumbnail,title,content FROM articles WHERE category = '".$_POST["category"]."' and ok = 1";
try{
	$stmt = $conn->query($sql_query);
 
	if($stmt === false){
		die("Error executing the query: $sql_query");
	}
}catch (PDOException $e){
	echo 'error occurred';
	echo $e->getMessage();
}


    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo nl2br('<a href = "view_article.php?id='.$row["id"].'" style = "padding:10px 10px 20px 10px;"><button class="w3-hide-small w3-hide-medium w3-btn w3-white w3-border w3-border-blue w3-round-xlarge" style = "min-width:45%;max-width:45%;min-height:200px;max-height:200px;margin-top:10px;"><div class = "w3-half"><img src = "'.$row["thumbnail"].'" style="height:200px;width:auto"></img></div><div class = "w3-half">&emsp;'.$row["title"].'<br><br>&emsp;<h7 style = "font-size:10px;">'.$row["content"].'</h7></button></a>');
		echo nl2br('<a href = "view_article.php?id='.$row["id"].'" style = "padding:10px 10px 20px 10px;"><button class="w3-hide-large w3-btn w3-white w3-border w3-border-blue w3-round-xlarge" style = "min-width:100%;max-width:100%;min-height:200px;max-height:100px;margin-top:10px;"><div class = "w3-half"><img src = "'.$row["thumbnail"].'" style="height:100px;width:auto"></img></div><div class = "w3-half">&emsp;'.$row["title"].'<br><br>&emsp;<h7 style = "font-size:10px;">'.$row["content"].'</h7></button></a>');

	}
$conn = null;
$stmt = null;
?>
<hr>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>
<!-- Footer -->
<footer style = "background-color:#9c9d9d;">
<div class = "w3-third w3-container " style = "background-color:#b3ada9;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h4><u>Copyright</u></h4>
<h6>All articles are property
of their owners and it is TOTALLY FREE
to copy them as long as you give the owners proper credits
and put a link back to the source.
</h6>
</div>
<div class = "w3-third w3-container " style = "background-color:lightgrey;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h5><u>Connect with us</u></h5>
<a href = "https://www.facebook.com/marinelounge/" target = "_blank" title = "Facebook"><img src = "images/facebook_icon.ico" height = "40" width = "auto"></img></a>&emsp;&emsp;
<a href = "https://twitter.com/marine_lounge" target = "_blank" title = "Twitter"><img src = "images/twitter_icon.ico" height = "40" width = "auto"></img></a>&emsp;&emsp;
<a href = "http://marinelounge.blogspot.in/" target = "_blank" title = "Blogger"><img src = "images/blogger_icon.ico" height = "40" width = "auto"></img></a>
</div>
<div class = "w3-third w3-container " style = "background-color:#b3ada9;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h4><u>Reach Us </u><h4>
<h5>Sumit Narayan</h5>
<h6>Founder, Marine Lounge,</h6>
<h6>Hostel
IMU, Kolkata Campus,
Kolkata, WB, India
700 088
+91-9455780760</h6>
</div>
<p align = "center" style = "padding:px 0px 0px 0px;margin:0px 0px 0px 0px; font-size:12px;">made with ❤ by <a href = "http://www.keyboardsan.org" target = "_blank" style = "color:white;text-decoration:none;">keyboardsan.org</a></p>

</footer>
</body>
</html>
