<!DOCTYPE html>
<html lang="en">
<head>
  <title>Marinelounge</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta property="fb:pages" content="270738909976749" />
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}

html {
position: relative;
min-height: 100%;
}
body {
margin: 0 0 600px; /* bottom = footer height */
}
footer {
position: absolute;
left: 0;
bottom: 0;
width: 100%;
background-color:lightgrey;
-webkit-box-shadow: 0px -4px 3px rgba(50, 50, 50, 0.4);
}
</style>
</head>
<body>

<!-- Navbar -->
<ul class="w3-navbar w3-blue w3-card-2 w3-top w3-left-align w3-large">
  <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large w3-hover-white w3-large w3-blue" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
  </li>
  <li><a href="index.php" class="w3-padding-large w3-white">Home</a></li>
				
				<li class="w3-hide-small">
					<a href="about.php" class="w3-padding-large w3-hover-white">About Us</a>
				</li >
				<li class="w3-hide-small">
					<a href="contact.php" class="w3-padding-large w3-hover-white">Contact</a>
				</li>
				<li class="w3-hide-small">
					<a href="rules.php" class="w3-padding-large w3-hover-white">Rules</a>
				</li>
				<li class = "w3-hide-small" style = "float:right;">
					<a href="register.php" class="w3-padding-large w3-hover-white">Login</a>
				</li>
</ul>


<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:51px;">
  <ul class="w3-navbar w3-left-align w3-large w3-black">
    <li class="w3-padding-large">
					<a href="about.php">About</a>
				</li>
				<li class="w3-padding-large">
					<a href="newsletter.php">Newsletter</a>
				</li>
				<li class="w3-padding-large">
					<a href = "naval_arch.php">Naval arch</a>
				</li>
				<li class="w3-padding-large">
					<a href = "careers.php">Careers</a>
				</li>
				<li class="w3-padding-large">
					<a href="contact.php">Contact</a>
				</li>
				<li class="w3-padding-large">
					<a href="register.php">Register</a>
				</li>
  </ul>
</div>
<br><br><br><br><br><br><br><br><br>
<div class = "w3-centered">
<div align="center">
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '565720713638680',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
</div>
</div>
<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>
<!-- Footer -->
<br><br><br><br><br>
<footer style = "background-color:#9c9d9d;">
<div class = "w3-third w3-container " style = "background-color:#b3ada9;min-height:200px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h4><u>Copyright</u></h4>
<h6>All articles are property
of their owners and it is TOTALLY FREE
to copy them as long as you give the owners proper credits
and put a link back to the source.
</h6>
</div>
<div class = "w3-third w3-container " style = "background-color:lightgrey;min-height:200px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h5><u>Connect with us</u></h5>
<a href = "https://www.facebook.com/marinelounge/" target = "_blank" title = "Facebook"><img src = "images/facebook_icon.ico" height = "40" width = "auto"></img></a>&emsp;&emsp;
<a href = "https://twitter.com/marine_lounge" target = "_blank" title = "Twitter"><img src = "images/twitter_icon.ico" height = "40" width = "auto"></img></a>&emsp;&emsp;
<a href = "http://marinelounge.blogspot.in/" target = "_blank" title = "Blogger"><img src = "images/blogger_icon.ico" height = "40" width = "auto"></img></a>
</div>
<div class = "w3-third w3-container " style = "background-color:#b3ada9;min-height:200px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h4><u>Reach Us </u><h4>
<h5>Sumit Narayan</h5>
<h6>Founder, Marine Lounge,</h6>
<h6>Hostel
IMU, Kolkata Campus,
Kolkata, WB, India
700 088
+91-9455780760</h6>
</div>
<p align = "center" style = "padding:px 0px 0px 0px;margin:0px 0px 0px 0px; font-size:12px;">made with ❤ by <a href = "http://www.keyboardsan.org" target = "_blank" style = "color:white;text-decoration:none;">keyboardsan.org</a></p>

</footer>
</body>
</html>
