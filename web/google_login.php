<?php
session_start(); //session start
require_once('includes/config.php');
require_once ('libraries/Google/autoload.php');

//Insert your client ID and secret 
//You can get it from : https://console.developers.google.com/
$client_id = '898588165133-0jvj032qs5fiauj3klvelffhfu2s05qi.apps.googleusercontent.com'; 
$client_secret = 'fzdoPStxXivcqslamkfBZVEp';
$redirect_uri = 'http://marinelounge.in/google_login.php';

//database


//incase of logout request, just unset the session var
if (isset($_GET['logout'])) {
  unset($_SESSION['access_token']);
  unset($_SESSION['name']);
  unset($_SESSION['id']);
  unset($_SESSION['picture_url']);
  unset($_SESSION['profile']);
}

/************************************************
  Make an API request on behalf of a user. In
  this case we need to have a valid OAuth 2.0
  token for the user, so we need to send them
  through a login flow. To do this we need some
  information from our API console project.
 ************************************************/
$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->addScope("email");
$client->addScope("profile");

/************************************************
  When we create the service here, we pass the
  client to it. The client then queries the service
  for the required scopes, and uses that when
  generating the authentication URL later.
 ************************************************/
$service = new Google_Service_Oauth2($client);

/************************************************
  If we have a code back from the OAuth 2.0 flow,
  we need to exchange that with the authenticate()
  function. We store the resultant access token
  bundle in the session, and redirect to ourself.
*/
  
if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
  exit;
}

/************************************************
  If we have an access token, we can make
  requests, else we generate an authentication URL.
 ************************************************/
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);
  if($client->isAccessTokenExpired()) {

    $authUrl = $client->createAuthUrl();
    header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));

  }
} else {
  $authUrl = $client->createAuthUrl();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Marinelounge</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=icon href="marinelounge_icon.png">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta property="fb:pages" content="270738909976749" />
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}

footer {
background-color:lightgrey;
-webkit-box-shadow: 0px -4px 3px rgba(50, 50, 50, 0.4);
}
</style>
</head>
<body>

<!-- Navbar -->
<ul class="w3-navbar w3-blue w3-card-2 w3-top w3-left-align w3-large">
  <li class="w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large w3-hover-white w3-large w3-blue" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
  </li>
  <li><a href="index.php" class="w3-padding-large w3-white">Home</a></li>
				
				<li class="w3-hide-small w3-hide-medium">
					<a href="about.php" class="w3-padding-large w3-hover-white">About Us</a>
				</li >
				<li class="w3-hide-small w3-hide-medium">
					<a href="contact.php" class="w3-padding-large w3-hover-white">Contact</a>
				</li>
				<li class="w3-hide-small w3-hide-medium">
					<a href="rules.php" class="w3-padding-large w3-hover-white">Rules</a>
				</li>
				<li class = "w3-hide-small w3-hide-medium" style = "float:right;">
					<?php
						if(!(isset($_SESSION["id"]))) {
					echo '<a href="google_login.php" class="w3-padding-large w3-hover-white">Login</a>';
					} else {
					echo '<a href="dashboard.php" class = "w3-padding-large w3-hover-white"><img src = "'.$_SESSION["picture_url"].'" style = "border-radius:50%;" height = "25"></img>&emsp;Dashboard</a>';
					}
					?>
				</li>
</ul>


<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-top" style="margin-top:51px;">
  <ul class="w3-navbar w3-left-align w3-large w3-black">
    <li class="w3-padding-large">
					<a href="about.php">About</a>
				</li>
				<li class="w3-padding-large">
					<a href="newsletter.php">Newsletter</a>
				</li>
				<li class="w3-padding-large">
					<a href = "naval_arch.php">Naval arch</a>
				</li>
				<li class="w3-padding-large">
					<a href = "careers.php">Careers</a>
				</li>
				<li class="w3-padding-large">
					<a href="contact.php">Contact</a>
				</li>
				<li class="w3-padding-large">
					<a href="register.php">Register</a>
				</li>
  </ul>
</div>
<br><br><br><br><br><br>
<div align="center">
<?php		
if (isset($authUrl)){ 
	//show login url
	
        echo '<br><br>';
	echo '<a class="login" href="' . $authUrl . '"><img src="images/google_login.png" width = "200" height = "auto" /></a>';
	
	
} else {
	
	$user = $service->userinfo->get(); //get user info 
	
	// connect to database
	
$dsn = "pgsql:host=$host;port=5432;dbname=$db;user=$username;password=$password";
 
try{
	// create a PostgreSQL database connection
	$conn = new PDO($dsn);
 
	// display a message if connected to the PostgreSQL successfully
	if($conn){
	}
}catch (PDOException $e){
	// report error message
	echo $e->getMessage();
}



$sql_query = "SELECT * FROM users WHERE id = '".$user->id."'";
try{
	$stmt = $conn->query($sql_query);
 
	if($stmt === false){
		die("Error executing the query: $sql_query");
	}
}catch (PDOException $e){
	echo 'error occurred';
	echo $e->getMessage();
}

	$user_count = $stmt->rowCount();
	
	//show user picture
        echo '<br>';
	echo '<img src="'.$user->picture.'" width = "50" height = "auto" style = "border-radius:50%;" />';
        echo '<br><br>';
	if($user_count>0) //if user already exist change greeting text to "Welcome Back"
    {
        $_SESSION['name'] = $user->name;
        $_SESSION['id'] = $user->id;
        $_SESSION['picture_url'] = $user->picture;
        $_SESSION['profile'] = $user->link;
        echo 'Welcome back '.$user->name.'<br> <a href = "http://marinelounge.in/index.php" style = "color:#45769c;text-decoration:none;">Enter Site</a><br><a href="http://marinelounge.in/google_login.php?logout=1" style="color:#45769c;text-decoration:none;">Log Out</a>';
    }
	else //else greeting text "Thanks for registering"
	{ 
	$_SESSION['name'] = $user->name;
        $_SESSION['id'] = $user->id;
        $_SESSION['picture_url'] = $user->picture;
        $_SESSION['profile'] = $user->link;
        echo 'Hi '.$user->name.', Thanks for Registering! <br> <a href = "http://marinelounge.in/index.php" style = "color:#45769c;text-decoration:none;">Enter Site</a> <br> <a href="http://marinelounge.in/google_login.php?logout=1" style = "color:#45769c;text-decoration:none;">Log Out</a>';
			$sql_query = "INSERT INTO users (id, name, email, link, picture_link) VALUES (".$conn->quote($user->id).",".$conn->quote($user->name).",".$conn->quote($user->email).",".$conn->quote($user->link).",".$conn->quote($user->picture).")";
				   try{
	$stmt = $conn->query($sql_query);
 
	if($stmt === false){
		die("Error executing the query: $sql_query");
	}
}catch (PDOException $e){
	echo 'error occurred';
	echo $e->getMessage();
}
		
		
    }
}
$conn = null;
$stmt = null;
?>
</div>
<br><br><br><br><br><br><br><br>
<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>
<!-- Footer -->

<footer style = "background-color:#9c9d9d;">
<div class = "w3-third w3-container " style = "background-color:#b3ada9;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h4><u>Copyright</u></h4>
<h6>All articles are property
of their owners and it is TOTALLY FREE
to copy them as long as you give the owners proper credits
and put a link back to the source.
</h6>
</div>
<div class = "w3-third w3-container " style = "background-color:lightgrey;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h5><u>Connect with us</u></h5>
<a href = "https://www.facebook.com/marinelounge/" target = "_blank" title = "Facebook"><img src = "images/facebook_icon.ico" height = "40" width = "auto"></img></a>&emsp;&emsp;
<a href = "https://twitter.com/marine_lounge" target = "_blank" title = "Twitter"><img src = "images/twitter_icon.ico" height = "40" width = "auto"></img></a>&emsp;&emsp;
<a href = "http://marinelounge.blogspot.in/" target = "_blank" title = "Blogger"><img src = "images/blogger_icon.ico" height = "40" width = "auto"></img></a>
</div>
<div class = "w3-third w3-container " style = "background-color:#b3ada9;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h4><u>Reach Us </u><h4>
<h5>Sumit Narayan</h5>
<h6>Founder, Marine Lounge,</h6>
<h6>Hostel
IMU, Kolkata Campus,
Kolkata, WB, India
700 088
+91-9455780760</h6>
</div>
<p align = "center" style = "padding:px 0px 0px 0px;margin:0px 0px 0px 0px; font-size:12px;">made with ❤ by <a href = "http://www.keyboardsan.org" target = "_blank" style = "color:white;text-decoration:none;">keyboardsan.org</a></p>

</footer>
</body>
</html>
