<?php require('includes/config.php'); 

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); } 

//define page title
$title = 'Members Page';

//include header template
require('layout/header.php'); 
?>
<!DOCTYPE html>
<html>
<title>Marine Lounge</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}

</style>
<body>

<!-- Navbar -->
<ul class="w3-navbar w3-blue w3-card-2 w3-top w3-left-align w3-large">
  <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large w3-hover-white w3-large w3-blue" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
  </li>
  <li><a href="index.php" class="w3-padding-large w3-white">Home</a></li>
				<li class="w3-hide-small">
					<a href="about.php" class="w3-padding-large w3-hover-white">About</a>
				</li >
				<li class="w3-hide-small">
					<a href="newsletter.php" class="w3-padding-large w3-hover-white">Newsletter</a>
				</li>
				<li class="w3-hide-small">
					<a href = "naval_arch.php" class="w3-padding-large w3-hover-white">Naval arch</a>
				</li>
				<li class="w3-hide-small">
					<a href = "careers.php" class="w3-padding-large w3-hover-white">Careers</a>
				</li>
				<li class="w3-hide-small">
					<a href="contact.php" class="w3-padding-large w3-hover-white">Contact</a>
				</li>
				<li class = "w3-hide-small" style = "float:right;">
					<a href="register.php" class="w3-padding-large w3-hover-white">Login</a>

				</li>
</ul>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:51px;">
  <ul class="w3-navbar w3-left-align w3-large w3-black">
     <li class="w3-padding-large">
					<a href="about.php">About</a>
				</li>
				<li class="w3-padding-large">
					<a href="newsletter.php">Newsletter</a>
				</li>
				<li class="w3-padding-large">
					<a href = "naval_arch.php">Naval arch</a>
				</li>
				<li class="w3-padding-large">
					<a href = "careers.php">Careers</a>
				</li>
				<li class="w3-padding-large">
					<a href="contact.php">Contact</a>
				</li>
				<li class="w3-padding-large">
					<a href="register.php">Register</a>
				</li>
</div>
<div class="w3-row-padding">
<br><br>
	

	    
			
				<h1>Dashboard - Welcome <?php echo $_SESSION['username']; ?></h1>
				<p><a href='logout.php' class="button w3-btn w3-padding-16 w3-large w3-margin-top">Logout</a></p>
				<hr>
				<a href = "submit.php" class="button w3-btn w3-padding-16 w3-large w3-margin-top">Submit Article</a><br><br>
				<a href = "your_articles.php" class="button w3-btn w3-padding-16 w3-large w3-margin-top">Your articles</a>

		
	


</div>


</body>
</html>