<!DOCTYPE html>
<html>
<title>Marine Lounge</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}

</style>
<body>

<!-- Navbar -->
<ul class="w3-navbar w3-blue w3-card-2 w3-top w3-left-align w3-large">
  <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large w3-hover-white w3-large w3-blue" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
  </li>
  <li><a href="index.php" class="w3-padding-large w3-white">Home</a></li>
				<li class="w3-hide-small">
					<a href="about.php" class="w3-padding-large w3-hover-white">About</a>
				</li >
				<li class="w3-hide-small">
					<a href="newsletter.php" class="w3-padding-large w3-hover-white">Newsletter</a>
				</li>
				<li class="w3-hide-small">
					<a href = "naval_arch.php" class="w3-padding-large w3-hover-white">Naval arch</a>
				</li>
				<li class="w3-hide-small">
					<a href = "careers.php" class="w3-padding-large w3-hover-white">Careers</a>
				</li>
				<li class="w3-hide-small">
					<a href="contact.php" class="w3-padding-large w3-hover-white">Contact</a>
				</li>
				<li class = "w3-hide-small" style = "float:right;">
					<a href="register.php" class="w3-padding-large w3-hover-white">Login</a>

				</li>
</ul>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:51px;">
  <ul class="w3-navbar w3-left-align w3-large w3-black">
    <li class="w3-padding-large">
					<a href="about.php">About</a>
				</li>
				<li class="w3-padding-large">
					<a href="newsletter.php">Newsletter</a>
				</li>
				<li class="w3-padding-large">
					<a href = "naval_arch.php">Naval arch</a>
				</li>
				<li class="w3-padding-large">
					<a href = "careers.php">Careers</a>
				</li>
				<li class="w3-padding-large">
					<a href="contact.php">Contact</a>
				</li>
				<li class="w3-padding-large">
					<a href="register.php">Register</a>
				</li>
  </ul>
</div>
<div class="w3-row-padding">
	<hr><br>
		<?php
		require_once('includes/config.php');


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id,thumbnail,title FROM articles WHERE category = 'Naval Arch'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo '<a href = "view_article.php?id='.$row['id'].';"> <div class="w3-third w3-container w3-margin-bottom">
      <img src="'.$row['thumbnail'].'" style="width:300px;height:200px;" class="w3-hover-opacity">
      <div class="w3-container w3-white">
        <h3>'.$row['title'].'</h3>
      </div>
    </div>';
    }
}
$conn->close();
?>
</div>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>

</body>
</html>
