<?php require('includes/config.php'); 

//if logged in redirect to members page
if( $user->is_logged_in() ){ header('Location: memberpage.php'); } 

$stmt = $db->prepare('SELECT resetToken, resetComplete FROM members WHERE resetToken = :token');
$stmt->execute(array(':token' => $_GET['key']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

//if no token from db then kill the page
if(empty($row['resetToken'])){
	$stop = 'Invalid token provided, please use the link provided in the reset email.';
} elseif($row['resetComplete'] == 'Yes') {
	$stop = 'Your password has already been changed!';
}

//if form has been submitted process it
if(isset($_POST['submit'])){

	//basic validation
	if(strlen($_POST['password']) < 3){
		$error[] = 'Password is too short.';
	}

	if(strlen($_POST['passwordConfirm']) < 3){
		$error[] = 'Confirm password is too short.';
	}

	if($_POST['password'] != $_POST['passwordConfirm']){
		$error[] = 'Passwords do not match.';
	}

	//if no errors have been created carry on
	if(!isset($error)){

		//hash the password
		$hashedpassword = $user->password_hash($_POST['password'], PASSWORD_BCRYPT);

		try {

			$stmt = $db->prepare("UPDATE members SET password = :hashedpassword, resetComplete = 'Yes'  WHERE resetToken = :token");
			$stmt->execute(array(
				':hashedpassword' => $hashedpassword,
				':token' => $row['resetToken']
			));

			//redirect to index page
			header('Location: login.php?action=resetAccount');
			exit;

		//else catch the exception and show the error.
		} catch(PDOException $e) {
		    $error[] = $e->getMessage();
		}

	}

}

//define page title
$title = 'Reset Account';

//include header template
require('layout/header.php'); 
?>
<!DOCTYPE html>
<html>
<title>Marine Lounge</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}

</style>
<body>

<!-- Navbar -->
<ul class="w3-navbar w3-blue w3-card-2 w3-top w3-left-align w3-large">
  <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large w3-hover-white w3-large w3-blue" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
  </li>
  <li><a href="index.php" class="w3-padding-large w3-white">Home</a></li>
				<li class="w3-hide-small">
					<a href="about.php" class="w3-padding-large w3-hover-white">About</a>
				</li >
				<li class="w3-hide-small">
					<a href="newsletter.php" class="w3-padding-large w3-hover-white">Newsletter</a>
				</li>
				<li class="w3-hide-small">
					<a href = "naval_arch.php" class="w3-padding-large w3-hover-white">Naval arch</a>
				</li>
				<li class="w3-hide-small">
					<a href = "careers.php" class="w3-padding-large w3-hover-white">Careers</a>
				</li>
				<li class="w3-hide-small">
					<a href="contact.php" class="w3-padding-large w3-hover-white">Contact</a>
				</li>
				<li class = "w3-hide-small" style = "float:right;">
					<a href="register.php" class="w3-padding-large w3-hover-white">Login</a>

				</li>
</ul>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:51px;">
  <ul class="w3-navbar w3-left-align w3-large w3-black">
     <li class="w3-padding-large">
					<a href="about.php">About</a>
				</li>
				<li class="w3-padding-large">
					<a href="newsletter.php">Newsletter</a>
				</li>
				<li class="w3-padding-large">
					<a href = "naval_arch.php">Naval arch</a>
				</li>
				<li class="w3-padding-large">
					<a href = "careers.php">Careers</a>
				</li>
				<li class="w3-padding-large">
					<a href="contact.php">Contact</a>
				</li>
				<li class="w3-padding-large">
					<a href="register.php">Register</a>
				</li>
</div>

	<div class="w3-row-padding">    

<br><br>
	    	<?php if(isset($stop)){

	    		echo "<p class='bg-danger'>$stop</p>";

	    	} else { ?>

				<form role="form" method="post" action="" autocomplete="off">
					<h2>Change Password</h2>
					<hr>

					<?php
					//check for any errors
					if(isset($error)){
						foreach($error as $error){
							echo '<p class="bg-danger">'.$error.'</p>';
						}
					}

					//check the action
					switch ($_GET['action']) {
						case 'active':
							echo "<h2 class='bg-success'>Your account is now active you may now log in.</h2>";
							break;
						case 'reset':
							echo "<h2 class='bg-success'>Please check your inbox for a reset link.</h2>";
							break;
					}
					?>

					<div class="row">
						
							<div class="form-group">
								<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="1">
							</div><br><br>
						
						
							<div class="form-group">
								<input type="password" name="passwordConfirm" id="passwordConfirm" class="form-control input-lg" placeholder="Confirm Password" tabindex="1">
							</div><br><br>
						
					</div>
					
					<hr>
					<div class="row">
						<input type="submit" name="submit" value="Change Password" class="btn btn-primary btn-block btn-lg" tabindex="3">
					</div>
				</form>

			<?php } ?>
		
	</div>

<?php 
//include header template
require('layout/footer.php'); 
?>
</body>
</html>