<?php
session_start();
require_once('includes/config.php');
if((isset($_SESSION['key']))) {
}
else {
header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Marine Lounge</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel=icon href="marinelounge_icon.png">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<script src="https://apis.google.com/js/platform.js" async defer>
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta property="fb:pages" content="270738909976749" />
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
footer {
background-color:lightgrey;
-webkit-box-shadow: 0px -4px 3px rgba(50, 50, 50, 0.4);
}
</style>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Navbar -->
<ul class="w3-navbar w3-blue w3-card-2 w3-top w3-left-align w3-large">
  <li class="w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large w3-hover-white w3-large w3-blue" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
  </li>
  <li><a href="index.php" class="w3-padding-large w3-white">Home</a></li>
				
				<li class="w3-hide-small w3-hide-medium">
					<a href="about.php" class="w3-padding-large w3-hover-white">About Us</a>
				</li >
				<li class="w3-hide-small w3-hide-medium">
					<a href="contact.php" class="w3-padding-large w3-hover-white">Contact</a>
				</li>
				<li class="w3-hide-small w3-hide-medium">
					<a href="rules.php" class="w3-padding-large w3-hover-white">Rules</a>
				</li>
				<li class = "w3-hide-small w3-hide-medium" style = "float:right;">
					<?php
						if(!(isset($_SESSION["id"]))) {
					echo '<a href="google_login.php" class="w3-padding-large w3-hover-white">Login</a>';
					} else {
					echo '<a href="dashboard.php" class = "w3-padding-large w3-hover-white"><img src = "'.$_SESSION["picture_url"].'" style = "border-radius:50%;" height = "25"></img>&emsp;Dashboard</a>';
					}
					?>
				</li>
</ul>


<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-top" style="margin-top:51px;">
  <ul class="w3-navbar w3-left-align w3-large w3-black">
    <li class="w3-padding-large">
					<a href="about.php">About Us</a>
				</li>
				<li class="w3-padding-large">
					<a href="contact.php">Contact</a>
				</li>
				<li class="w3-padding-large">
					<a href="rules.php">Rules</a>
				</li>
				<li class="w3-padding-large">
					<?php
						if(!(isset($_SESSION["id"]))) {
					echo '<a href="google_login.php" class="w3-padding-large">Login</a>';
					} else {
					echo '<a href="dashboard.php" class = "w3-padding-large"><img src = "'.$_SESSION["picture_url"].'" style = "border-radius:50%;" height = "25"></img>&emsp;Dashboard</a>';
					}
					?>
				</li>
  </ul>
</div>
<div class = "w3-row-padding">	
<?php


// Create connection
$dsn = "pgsql:host=$host;port=5432;dbname=$db;user=$username;password=$password";
 
try{
	// create a PostgreSQL database connection
	$conn = new PDO($dsn);
 
	// display a message if connected to the PostgreSQL successfully
	if($conn){
	}
}catch (PDOException $e){
	// report error message
	echo $e->getMessage();
}
$sql_query = "SELECT id,submittedby FROM articles WHERE id = '".$_GET["id"]."' and ok = 1";
try{
	$stmt = $conn->query($sql_query);
 
	if($stmt === false){
		die("Error executing the query: $sql_query");
	}
}catch (PDOException $e){
	echo 'error occurred';
	echo $e->getMessage();
}
    
   while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    	$submittedby = $row["submittedby"];
    	}

$sql_query = "SELECT name,link FROM users WHERE id = '".$submittedby."'";
try{
	$stmt = $conn->query($sql_query);
 
	if($stmt === false){
		die("Error executing the query: $sql_query");
	}
}catch (PDOException $e){
	echo 'error occurred';
	echo $e->getMessage();
}

    
   while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    	$link = $row["link"];
    	$name = $row["name"];
    	}
    
$sql_query = "SELECT id,thumbnail,title,content,category,submittedby,firstimage,secondimage,video,ok,featured,popular,favourite FROM articles WHERE id = '".$_GET["id"]."'";
try{
	$stmt = $conn->query($sql_query);
 
	if($stmt === false){
		die("Error executing the query: $sql_query");
	}
}catch (PDOException $e){
	echo 'error occurred';
	echo $e->getMessage();
}

$title = "";

    // output data of each row
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $title = $row['title'];
        $ok = $row["ok"];
        $featured = $row["featured"];
        $popular = $row["popular"];
        $favourite = $row["favourite"];
        $id = $row['id'];
		echo '<br><br><br>
<button class = "w3-btn w3-black">'.$row["category"].'</button>
<hr><a href = "view_article.php?id='.$row['id'].';"></a> <h1>'.$row['title'].'</h1><div class="w3-third w3-container w3-margin-bottom">
      <img src="'.$row['thumbnail'].'" height = "100" width = "auto" class="w3-hover-opacity"><p><b>Submitted By:</b><br><a href = "'.$link.'" style = "text-decoration:none;color:#45769c;">'.$name.'</a><br>';
	  echo nl2br('<p>'.$row["content"].'<p><br><br>');
		echo '<div class="fb-share-button" 
		data-href="http://marinelounge.in/view_article.php?id='.$row['id'].'" 
		data-layout="button_count">
	</div><br><br><div class="g-plus" data-action="share" data-height="24"></div></div>';
		echo '<div class="w3-third w3-container w3-margin-bottom"><br><br><img src = "'.$row["firstimage"].'" onerror = "this.src=&quot;images/blank.png&quot;" style = "max-width:100%;height:100%;"></img><br><br><br><br>
		
		<img src = "'.$row["secondimage"].' " style="max-width:100%;height:100%;" onerror = "this.src=&quot;images/blank.png&quot;"></img><br><br><br>';
		echo '<iframe src="'.$row['video'].'"
   style = "max-width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
    </div>';
    }

$conn = null;
$stmt = null;
?>
<form action = "admin_upload.php?id=<?php echo $id; ?>" method = "post">
Featured:
<select name = "featured" id = "featured" value ="<?php echo $featured; ?>" >
<option selected><?php echo $featured; ?></option>
<option>0</option>
<option>1</option>
</select>
<br><br>
<br>&emsp;&emsp;
Ok:
<select name = "ok" id = "ok" value ="<?php echo $ok; ?>" >
<option selected><?php echo $ok; ?></option>
<option>0</option>
<option>1</option>
</select>
<br><br>
<br>&emsp;
Popular:
<select name = "popular" id = "popular" value ="<?php echo $popular; ?>" >
<option selected><?php echo $popular; ?></option>
<option>0</option>
<option>1</option>
</select>
<br><br>
<br>&emsp;
Favourite:
<select name = "favourite" id = "favourite" value ="<?php echo $favourite; ?>" >
<option selected><?php echo $favourite; ?></option>
<option>0</option>
<option>1</option>
</select>
<br><br>
<br>&emsp;
<input type = "submit" name = "submit" id = "submit">
</form>
</div>
<div id="disqus_thread"></div>
<script>

/**
 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables */

var disqus_config = function () {
    //this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = $_GET["id"]; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};

(function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = '//marinelounge.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                    
</div>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>
<script id="dsq-count-scr" src="//marinelounge.disqus.com/count.js" async></script>
<!-- Footer -->
<footer style = "background-color:#9c9d9d;">
<div class = "w3-third w3-container " style = "background-color:#b3ada9;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h4><u>Copyright</u></h4>
<h6>All articles are property
of their owners and it is TOTALLY FREE
to copy them as long as you give the owners proper credits
and put a link back to the source.
</h6>
</div>
<div class = "w3-third w3-container " style = "background-color:lightgrey;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h5><u>Connect with us</u></h5>
<a href = "https://www.facebook.com/marinelounge/" target = "_blank" title = "Facebook"><img src = "images/facebook_icon.ico" height = "40" width = "auto"></img></a>&emsp;&emsp;
<a href = "https://twitter.com/marine_lounge" target = "_blank" title = "Twitter"><img src = "images/twitter_icon.ico" height = "40" width = "auto"></img></a>&emsp;&emsp;
<a href = "http://marinelounge.blogspot.in/" target = "_blank" title = "Blogger"><img src = "images/blogger_icon.ico" height = "40" width = "auto"></img></a>
</div>
<div class = "w3-third w3-container " style = "background-color:#b3ada9;min-height:260px;-webkit-box-shadow: 3px 3px 3px rgba(50, 50, 50, 0.4);">
<h4><u>Reach Us </u><h4>
<h5>Sumit Narayan</h5>
<h6>Founder, Marine Lounge,</h6>
<h6>Hostel
IMU, Kolkata Campus,
Kolkata, WB, India
700 088
+91-9455780760</h6>
</div>
<p align = "center" style = "padding:px 0px 0px 0px;margin:0px 0px 0px 0px; font-size:12px;">made with ❤ by <a href = "http://www.keyboardsan.org" target = "_blank" style = "color:white;text-decoration:none;">keyboardsan.org</a></p>

</footer>
</body>
</html>
